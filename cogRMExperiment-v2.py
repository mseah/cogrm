import copy
import random
import string


def getOutcome(di, op, trial, trials, resp):

    if resp == "j":
        #stationary case
        #so we return 0 or 500 with 50% probability
        return 500 * random.randint(0,1)

    else:
        #they chose f, so...
        #non-stationary case
        if di == 0:
            #decreasing case
            if op == 0:
                #outcome case
                #so we return 500 * trial/trials
                return int(500.0 - 500.0 * trial/float(trials))
            else:
                #prob case
                #so we return 500 with prob
                #1.0 - trial/trials
                p = 1.0 - trial/float(trials)
                if random.random() > p:
                    return 500
                else:
                    return 0
        else:
            #increasing case
            if op == 0:
                #outcome case
                return int(500.0 * trial/float(trials))
            else:
                #prob case  
                p = trial/float(trials)
                if random.random() > p:
                    return 500
                else:
                    return 0                
                
    
    

def run(di, op, trial, trials, numTries, fl):
    #numTries is to keep track of if the participant
    #did not click on J or F the first time,
    #and if it does not equal 1, then they see this:
    if numTries > 0:
        print "You must choose either J or F"
    resp = ""
    resp = raw_input("Pick option J or F: ")
    print "\tYou clicked " + resp 
    #(j is stationary, f is non-stationary)
    if resp not in string.ascii_letters:
        #if the participant didn't even click a letter,
        #tell them to try again!
        run(di, op, trial, trials, 1, fl)

    else:
      resp = string.lower(resp)
      if resp != "j" and resp != "f":
        #they need to choose J or F!
        run(di, op, trial, trials, 1, fl)

      else:
        #okay, we're here, meaning they chose one of the
        #two options... woohoo!

        #how much money did they earn from their
        #choice??
        outcome = getOutcome(di, op, trial, trials, resp)
        print "\tAnd you got " + str(outcome) + "\n"

        # Send to output
        output = "Trial %d resp %s outcome %s\n" % (trial, resp, outcome)
        fl.write(output)


def main():

    # Get subject info
    info = string.atoi(raw_input("Enter Subject ID: "))
    fl = open("outcomes.txt", "a")
    output = "Subject %s\n" % (info)
    fl.write(output)

    trials = 100
    decOrInc = 0
    #dec = 0, inc = 1
    outcomeOrProb = 0
    #outcome = 0, prob = 1

    trial = 0
    while trial < trials + 1:
        run(decOrInc, outcomeOrProb, trial, trials, 0, fl) 
        trial += 1

    fl.close()

main()


