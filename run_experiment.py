import copy
import datetime
import random
import string
import Tkinter as tk
from project import ExperimentApp


def getOutcome(di, op, trial, trials, resp):

    if resp == "j":
        #stationary case
        #so we return 0 or 500 with 50% probability
        return 500 * random.randint(0,1)

    else:
        #they chose f, so...
        #non-stationary case
        if di == 0:
            #decreasing case
            if op == 0:
                #outcome case
                #so we return 500 * trial/trials
                return int(500.0 - 500.0 * trial/float(trials))
            else:
                #prob case
                #so we return 500 with prob
                #1.0 - trial/trials
                p = 1.0 - trial/float(trials)
                if random.random() > p:
                    return 500
                else:
                    return 0
        else:
            #increasing case
            if op == 0:
                #outcome case
                return int(500.0 * trial/float(trials))
            else:
                #prob case  
                p = trial/float(trials)
                if random.random() > p:
                    return 500
                else:
                    return 0                

# Experiment completed 
def finish(Expt, s):
  Expt.finish(score = ("{:,d}".format(s)))

# Display outcome to participant
def intermediary(Expt, trial, trials, di, op, choice, outcome, fl, s):
  if choice == "j":
    resp = "J"
  else:
    resp = "F"
  txt = "\nYou chose %s...\n\nAnd you got %d." % (resp, outcome)
  txt += "\n\n\n\nTotal score: %s\n" % ("{:,d}".format(s)) #Comma formatting
  txt += "\n\n\nPress <space> to continue."
  Expt.toggle_canvas(False, text = txt)
  Expt.toggle_buttons(False, 
      fnM = lambda: run(Expt, trial, trials, di, op, fl, s))
                
def run(Expt, trial, trials, di, op, fl, s=0):
  def callback(Expt, trial, trials, di, op, choice, fl, s): 
    outcome = getOutcome(di, op, trial, trials, choice)
    trial += 1
    # Send to output
    output = "Trial %d resp %s outcome %s\n" % (trial, choice, outcome)
    fl.write(output)
    # Increment total score
    s += outcome
    if trial < trials:
      intermediary(Expt, trial, trials, di, op, choice, outcome, fl, s)
    else:
      finish(Expt, s)

  Expt.toggle_canvas(True, text = "Choose one:\n\n\n\nOption F\t\tOption J")
  Expt.toggle_buttons(True, 
      fnL = lambda: callback(Expt, trial, trials, di, op, "j", fl, s), 
      fnR = lambda: callback(Expt, trial, trials, di, op, "f", fl, s))

def start(Expt, trial, trials, di, op, fl):
  Expt.instructions(fnM = lambda: run(Expt, trial, trials, di, op, fl))

def main():

  decOrInc = random.randint(1, 100) % 2
  #dec = 0, inc = 1
  outcomeOrProb = random.randint(1, 100) % 2
  #outcome = 0, prob = 1

  # Get subject info
  info = raw_input("Enter Subject ID: ")

  # Create output text file
  today = datetime.date.today()
  datestamp = "%d.%d.%d" % (today.month, today.day, today.year)
  filename = "outcomes-%s-%s.txt" % (info, datestamp)

  fl = open(filename, "w+")
  output = "Subject %s\n" % (info)
  fl.write(output)

  trial = 0
  trials = 100

  output = "DecOrInc %d OutcomeOrProb %d\n" % (decOrInc, outcomeOrProb)
  fl.write(output)

  # Start tkinter
  root = tk.Tk()

  #root.geometry("100x100")
  Expt = ExperimentApp(root)
  #run(Expt, trial, trials, decOrInc, outcomeOrProb, fl)
  start(Expt, trial, trials, decOrInc, outcomeOrProb, fl)
  root.mainloop()
  # Experiment over
  fl.close()
  return

main()


