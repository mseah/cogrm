import Tkinter as tk

class ExperimentApp(object):
  def __init__(self, master, **kwargs):
    self.master = master
    self.master.title("85-310 Research Methods in Cognitive Psychology")
    
    # Make fullscreen
    pad = 3
    self._geom = '200x200+0+0'
    master.geometry("{0}x{1}+0+0".format(
      master.winfo_screenwidth()-pad, master.winfo_screenheight()-pad))
    master.configure(background = "black")

    # Bind keypresses
    master.bind('<Escape>', self.toggle_geom)            
    master.bind("J", self.chooseA)
    master.bind("j", self.chooseA)
    master.bind("F", self.chooseB)
    master.bind("f", self.chooseB)
    master.bind('<space>', self.space)

    # Initialize
    self.screenWidth = master.winfo_screenwidth()-pad
    self.bl = None
    self.br = None
    self.bm = None
    self.aFn = None
    self.bFn = None
    self.spaceFn = None

    # Set up canvas for text
    self.c = tk.Canvas(self.master, 
                  width = self.screenWidth, 
                  height = 500, 
                  highlightbackground="black",
                  bg = "black")
    self.c.pack()
    self.cTxt = self.c.create_text((self.screenWidth / 2, 75),
                  anchor = "center",
                  text = "",
                  fill = "white",
                  font = ("Courier", 32))

  def space(self, event):
    if self.spaceFn is not None:
      fn = self.spaceFn
      self.spaceFn = None
      fn()

  def chooseA(self, event):
    if (self.aFn is not None) and (self.spaceFn is None):
      fn = self.aFn
      self.aFn = None
      fn()

  def chooseB(self, event):
    # Prevent key from being pressed multiple times or during transition
    if (self.bFn is not None) and (self.spaceFn is None):
      fn = self.bFn
      self.bFn = None
      fn()

  # Buttons: either continue button, or buttons for experiment
  def toggle_buttons(self, on, **kwargs):
    buttonPadx = 500
    buttonPady = 300
    if on:
      self.aFn = kwargs["fnL"]
      self.bFn = kwargs["fnR"]
      if self.bm is not None:
        self.bm.destroy()
        self.bm = None
    else:
      self.spaceFn = kwargs["fnM"]

  def instructions(self, **kwargs):
    buttonPadx = 500
    buttonPady = 300
    txt0 = "At each stage, you will be presented with\n"
    txt1 = "two options to choose from.\n"
    txt2 = "Each option will award you with a number of points.\n"
    txt3 = "The reward for each option can vary over time.\n"
    txt4 = "Your goal is to gain as many points as possible.\n\n\n"
    txt5 = "Please repeat these instructions to the experimenter\n"
    txt6 = "before continuing."
    txt = txt0 + txt1 + txt2 + txt3 + txt4 + txt5 + txt6
    self.cTxt = self.c.create_text((self.screenWidth / 2, 175),
                   anchor = "center",
                   text = txt,
                   fill = "white",
                   justify = "center",
                   font = ("Courier", 32))
    self.bm = tk.Button(self.master,
                   text="Continue", 
                   bg="black", 
                   highlightbackground="black",
                   command = kwargs["fnM"],
                   padx = 40,
                   pady = 40,
                   bd=10)
    self.bm.place(x = self.screenWidth / 2 - 60, y = buttonPady + 50)

  # Either displays instructions for each task or feedback
  def toggle_canvas(self, run, **kwargs):
    txt = kwargs["text"]
    if run:
      self.c.delete("all")
      self.cTxt = self.c.create_text((self.screenWidth / 2, 125),
                  anchor = "center",
                  text = txt,
                  justify = "center",
                  fill = "white",
                  font = ("Courier", 32))
    else:
      self.c.delete("all")
      self.cTxt = self.c.create_text((self.screenWidth / 2, 200),
                  anchor = "center",
                  text = txt,
                  justify = "center",
                  fill = "white",
                  font = ("Courier", 32))
    return

  # Finish everything; give exit instructions.
  def finish(self, **kwargs):
    if self.bl is not None:
      self.bl.destroy()
    if self.bm is not None:
      self.bm.destroy()
    if self.br is not None:
      self.br.destroy()
    score = kwargs["score"]
    txt = "Thank you for participating.\n\n"
    txt += "Your final score was %s\n\n\n\n" % (score)
    txt += "Press <Esc> to exit."
    self.c.delete("all")
    self.cTxt = self.c.create_text((self.screenWidth / 2, 200),
                justify = "center",
                text = txt,
                fill = "white",
                font = ("Courier", 32))

  def toggle_input(self, **kwargs):
    t = tk.Text(self.master)
    t.insert('1.0', "Hello.....")
    t.pack()

  # Enables esc to close
  def toggle_geom(self,event):
    geom=self.master.winfo_geometry()
    self.master.geometry(self._geom)
    self._geom=geom
    self.master.destroy()

